def csv_writer(data,file):
    with open(file, "wt") as result_file:
        for row in data:  # for each line in the file
            result_file.write(row)  # L'ajout de virgule transforme le fichier en séparateur logique qui sera reconnu en format .csv
            result_file.write("\n")
#Partie GlyConnect
#Clé PubMed
PubMed = {} #Le dictionnaire avec la clé
with open("GlyConnect_Database.csv", "rt") as reference: #
    for myline in reference:
        data_GlyConnect = myline.strip("\n").split(";")
        clef = data_GlyConnect[2]
        valeur = data_GlyConnect[0]+" "+data_GlyConnect[1]
        if clef not in PubMed.keys():
            archivage = []
            PubMed[clef]=archivage
        PubMed[clef].append(valeur)
print("GlyConnect has "+str(len(PubMed.keys()))+" references.")
#Partie UniProt
PubMed_UniProt = {} #Le dictionnaire avec la clé
with open("results_human.txt", "rt") as reference_UniProt: #
    for query in reference_UniProt:
        data_UniProt = query.strip("\n").split(",")
        clef_UniProt = data_UniProt[3]
        valeur_UniProt = data_UniProt[0]+" "+data_UniProt[1]
        if clef_UniProt not in PubMed_UniProt.keys():
            archivage_UniProt = []
            PubMed_UniProt[clef_UniProt]=archivage_UniProt
        PubMed_UniProt[clef_UniProt].append(valeur_UniProt)
print("UniProt has "+str(len(PubMed_UniProt.keys()))+" references.")
#*************
#On compare les deux dictionnaires (GlyConnect VS UniProt)
CrossRef = []
Nenot = []
Nenot_2 = []
for PMID in PubMed.keys():
    if PMID in PubMed_UniProt.keys():
        CrossRef.append(PMID)
    else:
        Nenot.append(PMID)
print("There are "+str(len(CrossRef))+" references in common :", CrossRef)
print("The Glyconnect references that are not in UniProt are: "+str(Nenot))
#test de la protéine & du site
for PMID_2 in PubMed_UniProt.keys():
    if PMID_2 not in PubMed.keys():
        Nenot_2.append(PMID_2)
print("The UniProt references that are not in Glyconnect are: "+str(Nenot_2))
csv_writer(Nenot_2,"comparaison.csv")