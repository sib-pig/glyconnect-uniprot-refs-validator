mylines = [] #Déclare un liste vide
with open("Glyco_9430.txt", "rt") as myfile: #Ouvre le fichier Glyco_XXXX.txt et le lit comme un fichier texte
    for myline in myfile: #Pour toutes les lignes dans "myfile", aka le fichier ouvert Glyco_XXXX.txt
        mylines.append(myline.rstrip("\n")) #Éclate les lignes et les ajoute dans la liste

ID = "ID   "
substrat = "FT   CARBOHYD"
publication = "PubMed:"
index = -1
pattern = "FT                   "
Results = []
#On parcourt le fichier
for line in mylines:
    index += 1
    #On cherche le nom de la protéine/gène
    if line.startswith(ID):
        protein = line.replace("ID   ","") #On retire le début de ligne comportant la section "ID   "
        space_position = protein.index(" ") #On cherche le premier espace de la ligne
        protein = protein[0:space_position] #Permet de sélectionner le premier caractère de la ligne jusqu'au premier espace.
    #On cherche la glycosylation
    elif substrat in line:
        site = line.replace("FT   CARBOHYD        ", "")
        #On cherche l'acide aminé concerné
        PTM = mylines[index+1].replace("FT                   /note=", "")
        #On cherche, idéalement, la référence PubMed
        #Créer un index secondaire pour sortir les références, tant que l'on est dans "FT                   "
        j = index+2
        while pattern in mylines[j]:
            if publication in mylines[j]:
                PM_ID = mylines[j].split(publication)[1]
                Virgule_position = PM_ID.find(",") #On cherche la première virgule de la ligne
                PM_ID = PM_ID[0:Virgule_position] #Permet de sélectionner le premier caractère de la ligne jusqu'à la première virgule

                Row = [protein, site, PTM, PM_ID]
                Results.append(Row)
            j += 1
#Convertir le travail en fichier exploitable (.txt)
with open("results_9430.txt", "wt") as result_file:
    for row in Results:  # for each line in the file
        for value in row:
           result_file.write(value+",") #L'ajout de virgule transforme le fichier en séparateur logique qui sera reconnu en format .csv
        result_file.write("\n")
print("Fichier créé")